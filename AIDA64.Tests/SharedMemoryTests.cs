﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AIDA64.SensorValues;
using System.Linq;

namespace AIDA64.Tests
{
    [TestClass]
    public class SharedMemoryTests
    {
        [TestMethod]
        public void GetValuesFromSharedMemTest()
        {
            var values = SharedMemoryValues.GetSensorValues();

            Assert.IsNotNull(values);

            Assert.IsTrue(values.Any());

            Assert.IsFalse(values.Any(e => e.Value == null));
            Assert.IsFalse(values.Any(e => e.Name == null));
            Assert.IsFalse(values.Any(e => e.Identifier == null));
            Assert.IsFalse(values.Any(e => e.Type == SensorTypes.Unknown));
        }
    }
}