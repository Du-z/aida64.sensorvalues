﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace AIDA64.SensorValues
{
    public static class RegistryValues
    {
        private const string RegistryPath = @"Software\FinalWire\AIDA64\SensorValues";

        public static List<SensorValue> GetSensorValues()
        {
            RegistryKey mainKey = Registry.CurrentUser.OpenSubKey(RegistryPath);

            if (mainKey == null)
                return null;

            var valueNames = mainKey.GetValueNames();
            
            Dictionary<string, SensorValue> sensorValueList = new Dictionary<string, SensorValue>();

            foreach (var name in valueNames.Where(e => e.StartsWith("Label")))
            {
                var splitName = name.Split('.');
                var sensorName = splitName.LastOrDefault();

                var v = new SensorValue
                {
                    Identifier = sensorName.ToUpperInvariant(),
                    Type = SensorTypeStrings.GetTypeFromChar(sensorName[0]),
                    Name = Registry.GetValue(mainKey.Name, name, "") as string
                };

                sensorValueList[sensorName] = v;
            }

            foreach (var name in valueNames.Where(e => e.StartsWith("Value")))
            {
                var splitName = name.Split('.');
                var sensorName = splitName.LastOrDefault();
                
                sensorValueList[sensorName].Value = Registry.GetValue(mainKey.Name, name, "") as string;
            }

            return sensorValueList.Values.ToList();
        }

        
    
    }
}
