﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AIDA64.SensorValues
{
    public static class SharedMemoryValues
    {
        private const string SharedMemName = @"AIDA64_SensorValues";

        public static List<SensorValue> GetSensorValues()
        {
            var sensorValueList = new List<SensorValue>();

            try
            {
                using (var mmf = MemoryMappedFile.OpenExisting(SharedMemName))
                {
                    using (var stream = mmf.CreateViewStream())
                    {
                        using (BinaryReader binReader = new BinaryReader(stream))
                        {
                            var sb = new StringBuilder((int) stream.Length);
                            sb.Append("<root>");
                            var c = binReader.ReadChar();
                            while (c != '\0')
                            {
                                sb.Append(c);
                                c = binReader.ReadChar();
                            }
                            sb.Append("</root>");

                            var sharedMemString = sb.ToString();

                            var document = XDocument.Parse(sharedMemString);

                            foreach (var element in document.Root.Elements())
                            {
                                var v = new SensorValue();

                                v.Type = SensorTypeStrings.GetTypeFromStringCode(element.Name.LocalName);

                                foreach (var childElement in element.Elements())
                                {
                                    if (childElement.Name.LocalName == "id")
                                    {
                                        v.Identifier = childElement.Value;
                                    }
                                    else if (childElement.Name.LocalName == "label")
                                    {
                                        v.Name = childElement.Value;
                                    }
                                    else if (childElement.Name.LocalName == "value")
                                    {
                                        v.Value = childElement.Value;
                                    }
                                }

                                sensorValueList.Add(v);
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                return null;
            }

            return sensorValueList;
        }
    }
}
