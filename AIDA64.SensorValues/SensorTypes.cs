﻿
namespace AIDA64.SensorValues
{

    public enum SensorTypes
    {
        Unknown,
        CoolingFan,
        FanDuty,
        Temperature,
        Voltage,
        Current,
        Power,
        System,
    }

    public static class SensorTypeStrings
    {
        public const string FanDuty = "Fan Duty";
        public const string FanSpeed = "Fan Speed";
        public const string Temperature = "Temperature";
        public const string Voltage = "Voltage";
        public const string Current = "Current";
        public const string Power = "Power";
        public const string System = "System";
        public const string Unknown = "Unkown";

        internal static SensorTypes GetTypeFromChar(char typeChar)
        {
            switch (typeChar)
            {
                case 'F':
                    return SensorTypes.CoolingFan;
                case 'T':
                    return SensorTypes.Temperature;
                case 'V':
                    return SensorTypes.Voltage;
                case 'C':
                    return SensorTypes.Current;
                case 'P':
                    return SensorTypes.Power;
                case 'S':
                    return SensorTypes.System;
                case 'D':
                    return SensorTypes.FanDuty;
                default:
                    return SensorTypes.Unknown;
            }
        }

        internal static SensorTypes GetTypeFromStringCode(string typeString)
        {
            switch (typeString)
            {
                case "fan":
                    return SensorTypes.CoolingFan;
                case "temp":
                    return SensorTypes.Temperature;
                case "volt":
                    return SensorTypes.Voltage;
                case "curr":
                    return SensorTypes.Current;
                case "pwr":
                    return SensorTypes.Power;
                case "sys":
                    return SensorTypes.System;
                case "duty":
                    return SensorTypes.FanDuty;
                default:
                    return SensorTypes.Unknown;
            }
        }

        public static string GetStringForType (SensorTypes type)
        {
            switch (type)
            {
                case SensorTypes.CoolingFan:
                    return FanDuty;
                case SensorTypes.Temperature:
                    return Temperature;
                case SensorTypes.Voltage:
                    return Voltage;
                case SensorTypes.Current:
                    return Current;
                case SensorTypes.Power:
                    return Power;
                case SensorTypes.System:
                    return System;
                case SensorTypes.FanDuty:
                    return FanSpeed;
                default:
                    return Unknown;
            }
        }

    }
}
